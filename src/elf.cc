#include "elf.h"

Elf::Elf(int x, int y) : Player{x, y, 140} {
	setHp(140);
    setAtk(30);
    setDef(10);
}

void Elf::consume(Potion *p) {
    std::string potion = p->getPotionType();

    if (potion == "RH") {
        setHp(getHp() + 10);
    } else if (potion == "PH") {
        setHp(getHp() + 10);
    }

    // player = new BoostAtk(player);
    if (getHp() > maxHp) setHp(maxHp);
    if (getHp() < 0) setHp(0);
}

std::string Elf::getName() { return "Elf"; }
