#include "treasure.h"

Treasure::Treasure(int x, int y, int gold) : Item{x, y}, goldAmount{gold} {
    if (gold == 6) {
        setGuarded();
    } else {
        setUnguarded();
    }
 }

char Treasure::getSymbol() {
    return 'G';
}

int Treasure::getGoldAmount() {
    return this->goldAmount;
}


