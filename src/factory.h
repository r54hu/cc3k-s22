#ifndef _FACTORY_H_
#define _FACTORY_H_
#include "character.h"
#include "floor.h"
#include <vector>
#include <map>
#include "coordinates.h"
#include "item.h"
#include <random>

class Floor;

class SpawnFactory {
    std::map<int, std::vector<coordinates>> roomTiles;
    int seed;
    Floor *floor;
    std::default_random_engine rng;
	public: 
        SpawnFactory(std::map<int, std::vector<coordinates>> t, Floor *floor);
        SpawnFactory(Floor *floor);
        int randomInt(int low, int high);
        int randomChamber();
        coordinates randomTile(int chamber);
        //Character* spawnCharacter(char typeOfCharacter);
        Enemy* spawnEnemy(int chamber);
        Enemy* spawnDragon(std::vector<coordinates> coords);
        Item* spawnItem(char itemType);
        coordinates spawnPlayer(int room);
        
};


#endif
