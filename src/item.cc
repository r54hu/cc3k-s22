#include "item.h"
#include <iostream>

Item::Item(int x, int y) : xPos{x}, yPos{y} { }


int Item::getX() {
    return xPos; 
}

int Item::getY() {
    return yPos;
}

bool Item::isGuarded() { return guarded; }
void Item::setUnguarded() { guarded = false;}
void Item::setGuarded() { guarded = true; }
bool Item::isUsed() { return used; }
void Item::setUsed() { used = true; }


Item::~Item() {}
