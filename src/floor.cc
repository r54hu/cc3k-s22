#include <ostream>
#include <iostream>
#include "floor.h"
#include "dwarf.h"
#include "human.h"
#include "orc.h"
#include "elf.h"
#include "vampire.h"
#include "potion.h"
#include "dwarf.h"
#include "human.h"
#include "orc.h"
#include "elf.h"
#include "goblin.h"
#include "troll.h"
#include "merchant.h"
#include "phoenix.h"
#include "werewolf.h"
#include "dragon.h"
#include "compass.h"
#include <algorithm>
#include <random>
#include <chrono>
#include <vector>

using namespace std;

struct AttackDetails;

Floor::Floor() : seed{std::chrono::system_clock::now().time_since_epoch().count()} {
    player = nullptr;
    visibleStairs = false;
    map<int, vector<coordinates>> chambers;
    for (int chamber_number = 1; chamber_number <= 5; ++chamber_number) {
        vector<coordinates> insert_vector;
        chambers[chamber_number] = insert_vector;
    }

    for (int row = 0; row < 25; row ++) {
        std::vector<char> new_row;
        std::vector<char> char_row;
        for (int col = 0; col < 79; col ++) { 
            char_row.push_back(MAP_STR[row*79+col]);

            if(MAP_STR[row*79+col] == '1' || MAP_STR[row*79+col] == '2' || MAP_STR[row*79+col] == '3' ||MAP_STR[row*79+col] == '4' || MAP_STR[row*79+col] == '5') {
                chambers.at((int)MAP_STR[row*79+col]- 48).emplace_back(coordinates{col, row});
                new_row.push_back('.');
            } else {
                new_row.push_back(MAP_STR[row*79+col]);
            }
        }
        grid.push_back(new_row);
        MAP.push_back(char_row);

    }
    roomtiles = chambers;
    // generate seed (using time)
    spawnfactory = new SpawnFactory{roomtiles, this};

    generateFloor();
    
    
    updateDisplay(); 
}

Floor::Floor(string floorString, Player *p) : visibleStairs{false} {
    std::vector<char> new_row;
    for (int row = 0; row < 25; row++) {
        std::vector<char> grid_row;
        std::vector<char> map_row;

        for (int col = 0; col < 79; col ++) { 
            char tile = floorString[row*80 + col];
            grid_row.push_back(' ');
            if (tile == '+' || tile == '-' || tile == '|' || tile == '#'|| tile == ' ') {
                map_row.push_back(tile);
            } else {
                map_row.push_back('.');
            }

            switch (tile) {
                case '.':
                    break;
                case '@': {
                    player = p;
                    p->setX(col);
                    p->setY(row);                
                    break;
                }
                case 'V':
                    enemies.push_back(new Vampire(col, row));
                    break;
                case 'W':
                    enemies.push_back(new Werewolf(col, row));
                    break;
                case 'N':
                    enemies.push_back(new Goblin(col, row));
                    break;
                case 'M':
                    enemies.push_back(new Merchant(col, row));
                    break;
                case 'D':
                    //enemies.push_back(new Dragon(col, row));
                    // if (containsGold(row, col - 1)) {
                    //     for (auto g : items) {
                    //         if (g->getX() == col -1 && g->getY() == row) {
                    //             Treasure *t = dynamic_cast<Treasure *>(g);
                    //             if (t->getGoldAmount() == 6) {
                    //                 std::cout << "WOO";
                    //                 Dragon *d = dynamic_cast<Dragon *>(enemies.back());
                    //                 d->setHoard(g);
                    //             }
                    //         }
                    //     }
                    // }
                    // for (int i = -1; i <= 1; i++) {
                    //     for (int j = -1; j <= 1; j++) {
                    //         try {
                    //             if (containsGold(row + i, col + j)) {
                    //                 for (auto g : items) {
                    //                     if (g->getX() == col + j && g->getY() == row + i) {
                    //                         Treasure *t = dynamic_cast<Treasure *>(g);
                    //                         if (t->getGoldAmount() == 6) {
                    //                             std::cout << "WOO";
                    //                             Dragon *d = dynamic_cast<Dragon *>(enemies.back());
                    //                             d->setHoard(g);
                    //                         }
                    //                     }
                    //                 }
                    //             }   
                    //         } catch (...) {}
                    //     }
                    // }
                    break;
                case 'X':
                    enemies.push_back(new Phoenix(col, row));
                    break;
                case 'T':
                    enemies.push_back(new Troll(col, row));
                    break;
                case '0':
                    items.push_back(new Potion(col, row, "RH"));
                    break;
                case '1':
                    items.push_back(new Potion(col, row, "BA"));
                    break;
                case '2':
                    items.push_back(new Potion(col, row, "BD"));
                    break;
                case '3':
                    items.push_back(new Potion(col, row, "PH"));
                    break;
                case '4':
                    items.push_back(new Potion(col, row, "WA"));
                    break;
                case '5':
                    items.push_back(new Potion(col, row, "WD"));
                    break;
                case '6':
                    items.push_back(new Treasure(col, row, 1));
                    break;
                case '7':
                    items.push_back(new Treasure(col, row, 2));
                    break;
                case '8':
                    items.push_back(new Treasure(col, row, 4));
                    break;
                case '9':
                    items.push_back(new Treasure(col, row, 6));

                    break;
                case '\\':
                    stairCoords = coordinates{col, row};
                    
                    break;
                
                default:
                    // char_row.push_back(tile);
                    break;

            }
        }
        grid.push_back(grid_row);
        MAP.push_back(map_row);
    }
    
    spawnfactory = new SpawnFactory{this};
    
    updateDisplay();
    for (auto i : items) {
        Treasure *t = dynamic_cast<Treasure *>(i);
        if (t != nullptr && t->getGoldAmount() == 6) {
            //std::cout << "spawning a dragon " << std::endl;

            std::vector <coordinates> coords;

            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (canPlaceDragon(t->getY() + i, t->getX() + j)) {
                        // std::cout << "tatha test" << std::endl;
                        coords.push_back(coordinates{t->getY() + i, t->getX() + j});
                    }
                }
            }

        
            Dragon *d = dynamic_cast<Dragon *>(spawnfactory->spawnDragon(coords));
            updateDisplay();
            d->setHoard(t);
            enemies.push_back(d);
        } 
    }
}

void Floor::spawnStairs(int playerRoom) {
    int stairRoom = playerRoom;
    while (stairRoom == playerRoom) {
        stairRoom = spawnfactory->randomChamber();
    }
    stairCoords = spawnfactory->randomTile(stairRoom);
    visibleStairs = false;
}

void Floor::spawnBarrierSuit() {
    Item *bs = spawnfactory->spawnItem('b');
    items.push_back(bs);
   std::vector <coordinates> coords;

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (canPlaceDragon(bs->getY() + i, bs->getX() + j)) {
                coords.push_back(coordinates{bs->getY() + i, bs->getX() + j});
            }
        }
    }
    Dragon *d = dynamic_cast<Dragon *>(spawnfactory->spawnDragon(coords));
    updateDisplay();
    d->setHoard(bs);
    enemies.push_back(d);
}

void Floor::generateFloor() {
    // int rng = 1 + (rand() % 5);

    // generate the rng engine
    std::default_random_engine rng{seed};


    for (int i = 0; i < 20; i++) {
        // cout << x;
        enemies.push_back(spawnfactory->spawnEnemy(spawnfactory->randomChamber()));
    }

    // Spawn 10 potions
    for (int i = 0; i < 10; i++) {
        items.push_back(spawnfactory->spawnItem('p'));
    }

    // Spawn 10 potions
    for (int i = 0; i < 10; i++) {
        items.push_back(spawnfactory->spawnItem('t'));
    }

    // spawn compass
    items.push_back(spawnfactory->spawnItem('c')); 
    
    // if there is a dragon hoarde in items, spawn a dragon within its range
    for (auto i : items) {
        Treasure *t = dynamic_cast<Treasure *>(i);
        if (t != nullptr && t->getGoldAmount() == 6) {
            //std::cout << "spawning a dragon " << std::endl;

            std::vector <coordinates> coords;

            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (canPlaceDragon(t->getY() + i, t->getX() + j)) {
                        coords.push_back(coordinates{t->getY() + i, t->getX() + j});
                    }
                }
            }

            Dragon *d = dynamic_cast<Dragon *>(spawnfactory->spawnDragon(coords));
            updateDisplay();
            d->setHoard(t);
            enemies.push_back(d);
        } 
    }
    
    //update();
}

bool Floor::canPlaceDragon(int row, int col) {
    if (grid[row][col] == '.') return true;
    return false;
}

string getDirection(int fromRow, int fromCol, int toRow, int toCol) {
    if (toCol == fromCol && toRow == fromRow - 1) {
        return "North";
    } else if (toCol == fromCol && toRow == fromRow + 1) {
        return "South";
    } else if (toCol == fromCol + 1 && toRow == fromRow - 1) {
        return "North-East";
    } else if (toCol == fromCol - 1 && toRow == fromRow - 1) {
        return "North-West";
    } else if (toCol == fromCol + 1 && toRow == fromRow + 1) {
        return "South-East";
    } else if (toCol == fromCol - 1 && toRow == fromRow + 1) {
        return "South-West";
    } else if (toCol == fromCol + 1 && toRow == fromRow) {
        return "East";
    } else if (toCol == fromCol - 1 && toRow == fromRow) {
        return "West";
    }

    return "YO";
 }

 void Floor::seesPotionMessage(std::string &msg) {
    int row = player->getY();
    int col = player->getX();
    
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            if (i == 0 && j == 0) continue;
                try {
                    if (grid.at(row + i).at(col + j) == 'P') {

                                            
                            for (auto item : items) {
                            if (item->getX() == col + j && item->getY() == row + i) {
                                Potion *p = dynamic_cast<Potion *>(item);
                                if (p->usedPotionType()) {
                                    msg += string("PC sees a ") + p->getPotionType() + string(" potion ") + getDirection(row, col, row + i, col + j) + string(". ");
                                } else {
                                    msg += string("PC sees a potion ") + getDirection(row, col, row + i, col + j) + string(". ");
                                }
                            }
                        }
                    }
             } catch (...) {}
        }
    }
 }

Player *Floor::getPlayer() {
    return this->player;
}


bool Floor::containsPotion(int row, int col) {
    if (grid[row][col] == 'P') return true;
    return false;
}

bool Floor::containsGold(int row, int col) {
    if (grid.at(row).at(col) == 'G') return true;
    return false;
}

bool Floor::containsEntity(int row, int col, char symbol) {
    if (grid[row][col] == symbol) return true;
    return false;
}


void Floor::useItem(std::string direction, std::string &msg) {
    
    int x_dir = 0;
    int y_dir = 0;
    if (direction == "no") {
          y_dir = -1;
    }
    else if (direction == "nw") {
        y_dir = -1;
        x_dir = -1;
    } else if (direction == "ne") {
        y_dir = -1;
        x_dir = 1;
    } else if (direction == "so") {
        y_dir = 1;
    } else if (direction == "se") {
        y_dir = 1;
        x_dir = 1;
    } else if (direction == "sw") {
        y_dir = 1;
        x_dir = -1;
    } else if (direction == "ea") {
        x_dir = 1;
    } else if (direction == "we") {
        x_dir = -1;
    }

    if (containsPotion(player->getY() + y_dir, player->getX() + x_dir)) {
        bool elf = false;
        Elf *e = dynamic_cast<Elf *>(player);
        if (e != nullptr) elf = true;
        for (auto i : items) {
            if (i->getY() == player->getY() + y_dir && i->getX() == player->getX() + x_dir) {
                Potion *p = dynamic_cast<Potion *>(i);
                if (p->getPotionType() == "RH" || p->getPotionType() == "PH") {
                    player->consume(p);
                } else if (p->getPotionType() == "BA" || elf && p->getPotionType() == "WA") {
                    player = new BoostAtk{player};
                } else if (p->getPotionType() == "WA") {
                    player = new WoundAtk{player};
                } else if (p->getPotionType() == "BD" ||  elf && p->getPotionType() == "WD") {
                    player = new BoostDef{player};
                } else if (p->getPotionType() == "WD") {
                    player = new WoundDef{player};
                }
                p->setUsed();
                p->consume();

                msg = "PC used " + p->getPotionType() + ". ";
            }
        }
        //moveCharacter(direction, player);
        // player->consume();
    } else if (containsGold(player->getY() + y_dir, player->getX() + x_dir)) {
        for (auto i : items) {
            if (i->getY() == player->getY() + y_dir && i->getX() == player->getX() + x_dir) {
                Treasure *t = dynamic_cast<Treasure *>(i);
                if (!t->isGuarded()) {
                    player->lootGold(t);
                    t->setUsed();
                }
                

                msg = "PC picked up gold. ";
            }
        }
    } else if (containsEntity(player->getY() + y_dir, player->getX() + x_dir, 'C')) {
        visibleStairs = true;
        for (auto i : items) {
            if (i->getY() == player->getY() + y_dir && i->getX() == player->getX() + x_dir) {
                i->setUsed();
                msg = "PC picked up compass. ";
            }
        } 
    } else if (containsEntity(player->getY() + y_dir, player->getX() + x_dir, 'B')) {

        for (auto i : items) {
            if (i->getY() == player->getY() + y_dir && i->getX() == player->getX() + x_dir) {
                if (!(i->isGuarded())) {
                    player->equipBarrierSuit();
                    i->setUsed();
                    msg = "PC picked up barrier suit. ";
                } else {
                    msg = "PC did not pick up barrier suit. ";
                }
            }
        }
    } else { //
        throw 3;
    }

}

bool Floor::isTileOccupied(int x, int y) {
    for (Enemy *e : enemies) {
        if (e->isAlive()) {
            if (e->getY() == y && e->getX() == x) return true;
        }
    }
    for (Item *i : items) {
        if (!i->isUsed()) {
            if (i->getY() == y && i->getX() == x) return true;
        }
    }
    if (player != nullptr && player->getY() == y && player->getX() == x) return true;
    return false;
}

bool Floor::validMoveHuman(int y, int x) {
    return MAP[y][x] == '#' || validMove(y,x);
}

bool Floor::validMove(int y, int x) {

    if (isTileOccupied(x, y)) return false;

    char cell = MAP[y][x];
    if (!(cell == ' ' || cell == '|' || cell == '-' || cell == '#')) {
        return true;
    }
    return false;
}

void Floor::moveCharacter(std::string direction, Character *c) {

    int x_dir = 0;
    int y_dir = 0;
    if (direction == "no") {
        y_dir = -1;
    } else if (direction == "nw") {
        y_dir = -1;
        x_dir = -1;
    } else if (direction == "ne") {
        y_dir = -1;
        x_dir = 1;
    } else if (direction == "so") {
        y_dir = 1;
    } else if (direction == "se") {
        y_dir = 1;
        x_dir = 1;
    } else if (direction == "sw") {
        y_dir = 1;
        x_dir = -1;
    } else if (direction == "ea") {
        x_dir = 1;
    } else if (direction == "we") {
        x_dir = -1;
    }

    if (stairCoords.xPos == c->getX() + x_dir && stairCoords.yPos == c->getY() + y_dir) {
        throw 1337;
    }

    if (validMove(c->getY() + y_dir, c->getX() + x_dir) || (c->getSymbol() == '@' && validMoveHuman(c->getY() + y_dir, c->getX() + x_dir))) {
        grid[c->getY()][c->getX()] = (MAP[c->getY()][c->getX()] == '#' || MAP[c->getY()][c->getX()] == '+')? MAP[c->getY()][c->getX()] : '.';
        // c->setY();
        // c->setX();
        c->move(x_dir , y_dir);
        grid[c->getY()][c->getX()] = c->getSymbol();
    } else {
        throw 3;
    }

}

void Floor::attack(Character *attacker, std::string direction, std::string &msg) {
    // std::cout<<"henry is  a epdo";
    int x_dir = 0;
    int y_dir = 0;
    if (direction == "no") {
        y_dir = -1;
    } else if (direction == "nw") {
        y_dir = -1;
        x_dir = -1;
    } else if (direction == "ne") {
        y_dir = -1;
        x_dir = 1;
    } else if (direction == "so") {
        y_dir = 1;
    } else if (direction == "se") {
        y_dir = 1;
        x_dir = 1; 
    } else if (direction == "sw") {
        y_dir = 1;
        x_dir = -1;
    } else if (direction == "ea") {
        x_dir = 1;
    } else if (direction == "we") {
        x_dir = -1;
    }
    for (auto e : enemies) {
        if (e->withinRange(*player))  
            //std::cout << "p: " <<attacker->getX() + x_dir << " e: " << e->getX() << "p: " << attacker->getY() + y_dir << " e: "<< e->getY() << endl;
        if (e->isAlive() && (attacker->getX() + x_dir == e->getX()) && (attacker->getY() + y_dir == e->getY())) {
            auto victim = e;
            AttackDetails detail = attacker->attack(victim);
            string attackDamage = std::to_string(detail.damageDealt);
            string healthRemaing = std::to_string(detail.healthRemaning);
            

            msg += string("PC deals ") + attackDamage + string(" damage to ") + e->getSymbol() + string(" (") + healthRemaing + string(" HP). ");
            return;
            // grid[c->getY()][c->getX()] = c->getSymbol();
        } 
    }
    throw 10;


}

int Floor::randFloor() {
    return spawnfactory->randomChamber();   
}

void Floor::moveCharacterRandom(Character *c) {
    seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine rng{seed};
    string directions[] = {"no", "so", "ea", "we", "ne", "nw", "se", "sw"};
    std::shuffle(directions , directions  + sizeof(directions)/sizeof(directions[0]), rng);
    for (string dir: directions) {
        try {
            moveCharacter(dir, c);
            return;
        } catch (...) { }
    }
}


int Floor::spawnPlayer(Player *p) {
    player = p;
    int room = spawnfactory->randomChamber();
    coordinates coords = spawnfactory->randomTile(room);
    p->setX(coords.xPos);
    p->setY(coords.yPos);
    return room;
}

void Floor::updateDisplay() {
        for (int row = 0; row < 25; row ++) {
            for (int col = 0; col < 79; col ++) { 
                if(MAP[row][col] == '1' || MAP[row][col] == '2' || MAP[row][col] == '3' || MAP[row][col] == '4' || MAP[row][col] == '5') {
                    grid[row][col] = '.';
                } else if (MAP[row][col] == '!'){
                    grid[row][col] = '#';
                } else {
                    // cout << row << ", " << col<< endl;
                    char m =MAP[row][col];
                    grid[row][col] = m;
                }
            }
            for (Enemy *e : enemies) {
                if (e->isAlive()) {
                    grid[e->getY()][e->getX()] = e->getSymbol();
                } else {
                    //cout << e->getX()<< ", " << e->getY() << " " << e->getSymbol() << endl; 

                }
            }
            for (Item *i : items) {
                if (!i->isUsed()) {
                    grid[i->getY()][i->getX()] = i->getSymbol();
                } else {
                    // cout << i->getX() << ", " << i->getY() << " " << i->isUsed() << endl;
                }
            }
            if (player != nullptr) {
                if (player->isAlive()) {
                    grid[player->getY()][player->getX()] = player->getSymbol();
                }
            }
            if (visibleStairs) {
                    grid[stairCoords.yPos][stairCoords.xPos] = '\\';
            }
        }
}

void Floor::update(std::string &msg) {
    //cout << "update" << endl;

    sort( enemies.begin(), enemies.end(), [](Enemy* a, Enemy* b) {
        return a->getY() * 25 + a->getX() < b->getY() * 25 + b->getX(); 
    });
    for (Enemy *e : enemies) {
        if (e->isAlive()) {
            if (player != nullptr && e->withinRange(*player) && e->isHostile()) {
                //RYANS KOKe
                if (spawnfactory->randomInt(0,1) == 0) {
                    AttackDetails details = e->attack(player);
                    msg += e->getSymbol() + string(" deals ") + std::to_string(details.damageDealt) + string(" damage to PC. "); 
                }
                else msg += e->getSymbol() + string(" attack has missed! ");

            } else {
                moveCharacterRandom(e);
            }
        }
        // cout << e->getX()<< ", " << e->getY() << " " << e->getSymbol() << endl; 
    }

    updateDisplay();
    
}

Floor::~Floor() {
    for (auto i : items) {
        delete i;
    }
    for (auto j : enemies) {
        delete j;
    }
    delete spawnfactory;
}



std::ostream &operator<<(std::ostream &out, Floor &f){
    
    //coords print
    out << ' ';
    for (int x = 0; x < 79; x++); // out << x%10;
    out << '\n';
    char prev = '\0';
    for (int y = 0; y < 25; y++) {
        //out << y%10;
        for (int x = 0; x < 79; x++) {
            if (f.grid[y][x] == prev) {}
            else if (f.grid[y][x] == '@') 
                out << "\x1B[35m";
            else if (f.grid[y][x] == '-' || f.grid[y][x] == '|') 
                out << "\x1B[33m";
            else if (f.grid[y][x] == '.'|| f.grid[y][x] == '#'|| f.grid[y][x] == '+') 
                out << "\x1B[30m";
            else 
                out << "\033[0m";
            out << f.grid[y][x];
            prev = f.grid[y][x];
        }
        out << '\n';
        out << "\033[0m";
        prev = '\0';
    }
    return out;
}

#include "coordinates.h"

