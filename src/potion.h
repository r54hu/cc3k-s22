#ifndef _POTION_H
#define _POTION_H
#include "item.h"
#include <string>

class Potion : public Item {
    bool abilitiesVisible;
    std::string potionType;
    static bool seenRH;
    static bool seenPH;
    static bool seenBA;
    static bool seenWA;
    static bool seenBD;
    static bool seenWD;
    public:
        Potion(int x, int y, std::string potionType);
        std::string getPotionType();
        bool usedPotionType();
        void consume();
        char getSymbol() override;
};

#endif
