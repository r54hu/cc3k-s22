#include <ostream>
#include "game.h"
#include "dwarf.h"
#include "human.h"
#include "elf.h"
#include "orc.h"
#include <iostream>
#include <sstream>


Game::Game() : floor{nullptr}, floorNumber{0} {
    initPlayer();
    advanceFloor();
    barrierFloor = floor->randFloor();
    if (floorNumber == barrierFloor) {
        floor->spawnBarrierSuit();
        floor->updateDisplay();
        // std::cout <<"OOOOOOOOOOOOOOG BARRIER UIT FPOEOEEE";
    }
    //std::cout << "barrier floor: " << barrierFloor;
}

Game::Game(std::string map) : floor{nullptr}, floorNumber{0}, actionMessage{""}, preloadedMaps{true}{
    // cout << sstr.str();
    for (int i = 0; i < 5; i++) {
        std::string floorString = map.substr(i*80*25, 80*25);
        preloadedFloors[i] = floorString;
        // std::cout << i << floorString << std::endl << std::endl;
    }
    initPlayer();
    advanceFloorPreloaded();
}

void Game::initPlayer() { // get race from user and update player
    char race;
    std::cout << "Enter a race (human(h), orc(o), dwarf(d), elf(e)) : ";
    std::cin >> race;
    while (!(race == 'h' || race == 'o' || race == 'e' || race == 'd')) { // if user enters an invalid race, keep asking until they enter a valid race
        std::cout << "Invalid race, please try again. " << std::endl;
        std::cout << "Enter a race (human(h), orc(o), dwarf(d), elf(e)) : ";
        std::cin >> race;
    }
    //std::cout << std::endl;
    switch(race) {
        case 'h':
            // human
	        p = new Human{0, 0};
            break;
        case 'e':
            // elf
	        p = new Elf{0, 0};
            break;
        case 'd':
            // dwarf
            p = new Dwarf{0, 0};
	        break;
        case 'o':
            // orc
	        p = new Orc{0, 0};
            break;
        default:
            break;
    }
}

// TODO make undecorator a method

void Game::advanceFloorPreloaded() {
    delete floor;
    floor = new Floor(preloadedFloors[floorNumber], p);
    floorNumber++;
    // undecorate the player 
    Decorator *pp = dynamic_cast<Decorator *>(p);
    while (pp != nullptr) {
        p = pp->getComponent();
        pp = dynamic_cast<Decorator *>(p); 
    }

    // int playerRoom = floor->spawnPlayer(p); // sets player x and y spawn point
    floor->updateDisplay();
}

void Game::advanceFloor() {
    if (floorNumber == 5) Win();
    if (preloadedMaps) {
        advanceFloorPreloaded();
        return;
    }
    delete floor;
    floor = new Floor();
    floorNumber++;

    if (floorNumber == barrierFloor) floor->spawnBarrierSuit();

    // undecorate the player 
    Decorator *pp = dynamic_cast<Decorator *>(p);
    while (pp != nullptr) {
        p = pp->getComponent();
        pp = dynamic_cast<Decorator *>(p); 
    }

    int playerRoom = floor->spawnPlayer(p); // sets player x and y spawn point
    floor->spawnStairs(playerRoom); // spawns stairs
    floor->updateDisplay();
}

std::string Game::getActionMessage() {
    return actionMessage;
}

void Game::setActionMessage(std::string m) {
    actionMessage += m;
}

int Game::getScore() {
    return p->getScore();
}

void Game::resetActionMessage() {
   actionMessage = "";
}

void Game::move(std::string dir) {
    std::string potionMsg = "";
    try {
        floor->moveCharacter(dir, p);

    } catch (int x) {
        if ( x == 1337 ) {
            advanceFloor();
        }
        else 
            throw 3;
    }

    setActionMessage("PC moves " + dir + ". ");
    floor->seesPotionMessage(potionMsg);
    setActionMessage(potionMsg);
}

void Game::attack(std::string dir) {
    std::string msg = "";
    floor->attack(p, dir, msg);
    setActionMessage(msg);
}

void Game::useItem(std::string dir) {
    std::string msg = "";

    floor->useItem(dir, msg);
    move(dir);

    p = floor->getPlayer();

    setActionMessage(msg);

}

void Game::update() {
    std::string msg = "";
    floor->update(msg);
    setActionMessage(msg);
    if (!p->isAlive()) this->Lose();
}

void Game::Win() {
    throw 'W';
}

void Game::Lose() {
    throw 'L';
}

std::ostream &operator<<(std::ostream &out, const Game &g){
    out << *g.floor;
    out << "\x1B[96m";
    out << "\x1B[96m" << "Race: " << "\x1B[37m" << g.p->getName();
    out << "\x1B[96m" << " Gold: " << "\x1B[37m" << g.p->getGold() << "\x1B[96m" << "\t\t\t\t\t\t     Floor " << "\x1B[37m" << g.floorNumber << "\n";
    out << "\x1B[96m" << "HP: " << "\x1B[37m" << g.p->getHp() << '\n';
    out << "\x1B[96m" << "Atk: " << "\x1B[37m" << g.p->getAtk() << '\n';
    out << "\x1B[96m" <<  "Def: " << "\x1B[37m" << g.p->getDef() << '\n';
    out << "\x1B[96m" << "Action: " << "\x1B[37m" << g.actionMessage << '\n';
    out << "\x1B[31m";
    out << "\033[0m";

    return out;
}

Game::~Game() {
    delete floor;
    delete p;
}
