#include "potion.h"

Potion::Potion(int x, int y, std::string potionType) : Item{x, y}, potionType{potionType} { }

bool Potion::seenBA = false;
bool Potion::seenWA = false;
bool Potion::seenBD = false;
bool Potion::seenWD = false;
bool Potion::seenPH = false;
bool Potion::seenRH = false;


void Potion::consume() {
    if (potionType == "BA") {
        seenBA = true;
    } else if (potionType == "WA") {
        seenWA = true;
    } else if (potionType == "BD") {
        seenBD = true;
    } else if (potionType == "WD") {
        seenWD = true;
    } else if (potionType == "PH") {
        seenPH = true;
    } else if (potionType == "RH") {
        seenRH = true;
    }
}

char Potion::getSymbol() {
    return 'P';
}

std::string Potion::getPotionType() {
    return potionType;
}

bool Potion::usedPotionType() {
    if (potionType == "BA") {
        return seenBA;
    } else if (potionType == "WA") {
        return seenWA;
    } else if (potionType == "BD") {
        return seenBD;
    } else if (potionType == "WD") {
        return seenWD;
    } else if (potionType == "PH") {
        return seenPH;
    } else if (potionType == "RH") {
        return seenRH;
    }
    return false;
}



