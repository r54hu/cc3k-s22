#include <iostream>
#include <fstream>
#include <sstream>
#include "game.h"
using namespace std;

void commandHelp();
void invalidCommand();
void symbolList();

int main(int argc, char *argv[])  {
    Game *game;
    if (argc == 2) {
        string fn = argv[1];
        std::ifstream in(fn);
        std::ostringstream sstr;
        sstr << in.rdbuf();
        game = new Game{sstr.str()};
    } else {
        game = new Game{};
    }

    cout << *game;

    char cmd;
    bool quit = false;

    std::cout << endl;
    commandHelp();

    while (true && !quit) {
        
        game->resetActionMessage();

        cout << "Enter a move: ";
        std::cin >> cmd;

        char direction;
        std::string dir;

        try {
                switch(cmd) {

                   case 'n':
                        // move character north               
                        std::cin >> direction;

                        switch (direction) {
                            case 'o':
                                // character moves north fully
                                game->move("no");
                                break;
                            case 'w':
                                // move character north-west
                                game->move("nw");
                                break;
                            case 'e':
                                // move character north-east
                                game->move("ne");
                                break;
                        }
			            game->update();
                        cout << *game;
                        break;
                    case 's':
                        // move character south
                        std::cin >> direction;
                        
                        switch (direction) {
                            case 'o':
                                // character moves north fully
                                game->move("so");
                                break;
                            case 'w':
                                // move character north-west
                                game->move("sw");
                                break;
                            case 'e':
                                // move character north-east
                                game->move("se");
                                break;
                        }
                        game->update();
                        cout << *game;
                        break;
                    case 'e':
                        // move character east
                        std::cin >> direction;

                        switch (direction) {
                            case 'a':
                                // character moves north fully
                                game->move("ea");
                                break;
                        }
                        game->update();
                        cout << *game;
                        break;
                    case 'w':
                        // move character west
                        std::cin >> direction;

                        switch (direction) {
                            case 'e':
                                // character moves north fully
                                game->move("we");
                                break;
                        }
                        game->update();
                        cout << *game;
                        break;

                    case 'u': 
                        // use potion
                        std::cin >> dir;
                        try {
                            game->useItem(dir);
                            game->update();
                        } catch (...) {}
                        cout << *game;
                        break;
                    case 'a':
                        // attack enemy
                        std::cin >> dir;
                        game->attack(dir);
                        game->update();
                        cout << *game;
                        break;
                    case 'r':
                        // restart game
                        delete game;                        
                        cout << "Game was restarted! " << endl;
                        if (argc == 2) {
                            string fn = argv[1];
                            std::ifstream in(fn);
                            std::ostringstream sstr;
                            sstr << in.rdbuf();
                            game = new Game{sstr.str()};
                        } else {
                            game = new Game{};
                        }
                        cout << *game;
                        break;
                    case 'q':
                        // quit game
                        cout << "Thanks for playing! " << endl;
                        quit = true; 
                        break;
                    case 'h' :
                        // command help
                        commandHelp();
                        break;
                    case 'l' :
                        symbolList();
                        break;
                    default:
                        invalidCommand();
                        break;
                }
            } catch(int err) {
		    cout << "invalid move" << endl;;
	    } catch (char x) {
            cout << *game;

            if (x == 'L') {
                cout << "You lost wow!" << endl;
                cout << " Your Final Score : " << game->getScore() << endl;
                cout << "\x1B[31m";
            cout << 
    "|-----------------------------------------------------------------------------|\n\
|                                                                             |\n\
|                                                                             |\n\
|                                                                             |\n\
|                                                                             |\n\
|         ###                     ###############         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###                    ###          ###         ###                 |\n\
|         ###############         ###############         ###############     |\n\
|                                                                             |\n\
|                                                                             |\n\
|                                                                             |\n\
|                                                                             |\n\
|                                                                             |\n\
|-----------------------------------------------------------------------------|\n";
                cout << "LOSS" << endl;
                cout << "\x1B[37m";
            }
            if (x == 'W') {

                cout << "Score : " << game->getScore() << endl;
     
                cout << "\x1B[32m";

                cout << 
                    "|-----------------------------------------------------------------------------|\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |        ###############         ##############          ###############      |\n\
                |        ###         ###        ###          ###         ###          ###     |\n\
                |        ###         ###        ###          ###         ###                  |\n\
                |        ###         ###        ###          ###         ###                  |\n\
                |        ###         ###        ###          ###         ###                  |\n\
                |        ###         ###        ###          ###         ###                  |\n\
                |        ###         ###        ###          ###         ###                  |\n\
                |        ###############        ###          ###         ###                  |\n\
                |        ###                    ###          ###         ###         ####     |\n\
                |        ###                    ###          ###         ###           ##     |\n\
                |        ###                    ###          ###         ###           ##     |\n\
                |        ###                    ###          ###         ###           ##     |\n\
                |        ###                    ###          ###         ###           ##     |\n\
                |        ###                     ##############          ################     |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |                                                                             |\n\
                |-----------------------------------------------------------------------------|\n";
            }

            char again;
            cout << "Would you like to play again? (y/n) : ";
            cin >> again;

            if (again == 'y') {
                delete game;                        
                cout << "Game was restarted! " << endl;
                game = new Game{};
                cout << *game;
            } else {      
                quit = true;
            }
        }
            catch (...) {cout << "i like dudes";}
}

    delete game;
}

void commandHelp() {
    cout << "\x1B[32m" << "Commands: " << "\x1B[37m" << endl;
    cout << "no, nw, ne, so, sw, se, ea, we: move player in the designated direction" << endl;
    cout << "u <direction> : uses item in the designated direction" << endl;
    cout << "a <direction> : attacks character in the designated direction" << endl;
    cout << "r : restart" << endl;
    cout << "q : quit" << endl;
    cout << "h : help" << endl;
    cout << "l : symbol guide" << endl;
}

void invalidCommand() {
    cout << "Invalid command, try again or enter 'h' for help. " << endl;
}

void symbolList() {
    cout << "\x1B[32m" << "Symbols" << "\x1B[37m" << endl;
    cout << "@ : Player" << endl;
    cout << "V : Vamppire" << endl;
    cout << "W : Werewolf" << endl;
    cout << "N : Goblin" << endl;
    cout << "M : Merchant" << endl;
    cout << "D : Dragon" << endl;
    cout << "X : Phoenix" << endl;
    cout << "T : Troll" << endl;
    cout << "G : Gold" << endl;
    cout << "P : Potion" << endl;
    cout << "C : Compass" << endl;
    cout << "B : Barrier Suit" << endl;
}

