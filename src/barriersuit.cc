#include "barriersuit.h"

BarrierSuit::BarrierSuit(int x, int y) : Item{x, y} {
    setGuarded();
}

char BarrierSuit::getSymbol() {
    return 'B';
}
