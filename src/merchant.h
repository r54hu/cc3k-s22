#ifndef _MERCHANT_H
#define _MERCHANT_H

#include "enemy.h"

class Merchant : public Enemy {
    static bool hostile;
    public:
        Merchant(int x, int y);
        char getSymbol() const override;
        AttackDetails takeDamage(Character* attacker) override;
        bool isHostile() override;
        void makeHostile();

};

#endif
