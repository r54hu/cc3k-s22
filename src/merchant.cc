#include "merchant.h"
#include <cmath>

Merchant::Merchant(int x, int y) : Enemy{x, y} {
    setHp(30);
    setAtk(70);
    setDef(5);

}

bool Merchant::hostile = false; 

bool Merchant::isHostile() {
    return Merchant::hostile;
}

AttackDetails Merchant::takeDamage(Character* attacker)  {
    Merchant::hostile = true;
    double damage = (100.0/(100.0 + getDef())) * attacker->getAtk();
    int realDamage = std::ceil(damage);
    setHp(getHp() - realDamage);
    
    AttackDetails retval {realDamage, getHp()};

    return retval;
    
}


char Merchant::getSymbol() const {
    return 'M';
}
