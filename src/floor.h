#ifndef _FLOOR_H_
#define _FLOOR_H_

#include <vector>
#include <map>
#include <ostream>
#include "item.h"
#include "enemy.h"
#include "player.h"
#include "factory.h"
#include "boostatk.h"
#include "boostdef.h"
#include "woundatk.h"
#include "wounddef.h"

struct coordinates;

class Floor {
    bool visibleStairs;
    coordinates stairCoords;
    unsigned seed;
    Player *player;
    std::vector<Item *> items;
    std::vector<Enemy *> enemies; 
    std::vector<std::vector<char>> grid; 
    std::vector<std::vector<char>> MAP; 
    std::map<int, std::vector<coordinates>> roomtiles;
    SpawnFactory *spawnfactory;
    public:
        Floor();
        Floor(std::string floorString, Player *p);
        int randFloor();
        void setPlayer(char race);
        Player *getPlayer();
        void generateFloor();
        int spawnPlayer(Player *p);
        void update(std::string &msg);
        void updateDisplay();
        //bool playerSeesPotion();
        void seesPotionMessage(std::string &msg);
        void useItem(std::string dir, std::string &msg);
        bool containsPotion(int row, int col);
        bool containsGold(int row, int col);
        bool containsEntity(int row, int col, char symbol);
        bool canPlaceDragon(int row, int col);
        void spawnStairs(int playerRoom);
        void spawnBarrierSuit();
        bool validMove(int y, int x);
        bool validMoveHuman(int y, int x);
        bool isTileOccupied(int x, int y);
        void moveCharacter(std::string direction, Character *c);
        void moveCharacterRandom(Character *c);
        void spawnItems();
        void spawnEnemies();
        void generateBarrierSuit();
        void attack(Character *attacker, std::string direction, std::string &msg);
        coordinates generateCoord(int chamber_room);
        ~Floor();
        friend std::ostream &operator<<(std::ostream &out, Floor &f);

        // std::map<int, int> roomtiles[5];
        const char * MAP_STR = 
        "|-----------------------------------------------------------------------------|"
        "|                                                                             |"
        "| |--------------------------|        |-----------------------|               |"
        "| |11111111111111111111111111|        |22222222222222222222222|               |"
        "| |11111111111111111111111111+########+22222222222222222222222|-------|       |"
        "| |11111111111111111111111111|   #    |2222222222222222222222222222222|--|    |"
        "| |11111111111111111111111111|   #    |2222222222222222222222222222222222|--| |"
        "| |----------+---------------|   #    |----+----------------|222222222222222| |"
        "|            #                 #############                |222222222222222| |"
        "|            #                 #     |-----+------|         |222222222222222| |"
        "|            #                 #     |333333333333|         |222222222222222| |"
        "|            ###################     |333333333333|   ######+222222222222222| |"
        "|            #                 #     |333333333333|   #     |222222222222222| |"
        "|            #                 #     |-----+------|   #     |--------+------| |"
        "|  |---------+-----------|     #           #          #              #        |"
        "|  |444444444444444444444|     #           #          #         |----+------| |"
        "|  |444444444444444444444|     ########################         |55555555555| |"
        "|  |444444444444444444444|     #           #                    |55555555555| |"
        "|  |444444444444444444444|     #    |------+--------------------|55555555555| |"
        "|  |444444444444444444444|     #    |555555555555555555555555555555555555555| |"
        "|  |444444444444444444444+##########+555555555555555555555555555555555555555| |"
        "|  |444444444444444444444|          |555555555555555555555555555555555555555| |"
        "|  |---------------------|          |---------------------------------------| |"
        "|                                                                             |"
        "|-----------------------------------------------------------------------------|";
};

#endif
