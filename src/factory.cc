#include <algorithm>
#include <random>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include "floor.h"
#include "barriersuit.h"
#include "player.h"
#include "enemy.h"
#include "dwarf.h"
#include "human.h"
#include "orc.h"
#include "elf.h"
#include "vampire.h"
#include "goblin.h"
#include "troll.h"
#include "merchant.h"
#include "phoenix.h"
#include "potion.h"
#include "werewolf.h"
#include "compass.h"
#include "dragon.h" 

SpawnFactory::SpawnFactory(std::map<int, std::vector<coordinates>> t, Floor *floor) : 
roomTiles{t}, seed{std::chrono::system_clock::now().time_since_epoch().count()}, floor{floor}{
	std::random_device r; 
    std::default_random_engine prng(r());
    this->rng = prng;
};

SpawnFactory::SpawnFactory(Floor *floor) : 
seed{std::chrono::system_clock::now().time_since_epoch().count()}, floor{floor}{
	std::random_device r; 
    std::default_random_engine prng(r());
    this->rng = prng;
}

int SpawnFactory::randomInt(int low, int high) {
    std::uniform_int_distribution<int> dist(low, high);
    return dist(rng);


    std::vector<int> s;
    for (int i = low; i <= high; ++i) {
        s.emplace_back(i);
    }
    std::shuffle(s.begin(), s.end(), rng);
    //std::cout << s[0];
    return s[0];
}

int SpawnFactory::randomChamber() {
    return randomInt(1,5);
}

coordinates SpawnFactory::randomTile(int chamber) {
	std::vector<coordinates> room = roomTiles.at(chamber);
    std::shuffle(room.begin(), room.end(), rng);

    int i = 0;
    while (floor->isTileOccupied(room.at(i).yPos, room.at(i).xPos )) {
        i++;
    }    
    return room.at(i);    
}

coordinates SpawnFactory::spawnPlayer(int room) {
    coordinates coords = randomTile(room);
    return coords;
}

Enemy* SpawnFactory::spawnEnemy(int chamber) {
    coordinates coords = randomTile(chamber);
    //std::cout << chamber; 
    int x = coords.xPos;
    int y = coords.yPos;
    char enemyOdds[] = {'w','w','w','w','v','v','v','g','g','g','g','g','t','t','p','p','m','m'};
    std::shuffle(enemyOdds , enemyOdds + sizeof(enemyOdds)/sizeof(enemyOdds[0]), rng);
    // std::cout << enemyOdds[0];
    switch (enemyOdds[0]) {
        case 'w':
            return new Werewolf{x, y};
        case 'v':
            return new Vampire{x, y};
        case 'g':
            return new Goblin{x, y};
        case 't':
            return new Troll{x, y};
        case 'p':
            return new Phoenix{x, y};
        case 'm':
            return new Merchant{x, y};
        default:
            break;
    }

}

Enemy* SpawnFactory::spawnDragon(std::vector<coordinates> coords) {
    coordinates spawn = coords.at(this->randomInt(0, coords.size() - 1));

    //std::cout << "row = " << spawn.yPos << " col = " << spawn.xPos << std::endl; 
    return new Dragon{spawn.yPos, spawn.xPos};
}

Item* SpawnFactory::spawnItem(char itemType) { 
    seed = std::chrono::system_clock::now().time_since_epoch().count();
	// std::default_random_engine rng{seed};
    coordinates coords = randomTile(randomChamber());
    int x = coords.xPos;
    int y = coords.yPos;

    std::string potionOdds[] = {"RH","BA","BD","PH","WA","WD"};
    // std::string treasureOdds[] ={"D", "D", "D", "D", "D"}; 
    std::string treasureOdds[] = {"S","S","N","N","N","N","N","D"};

    switch(itemType) {
        case 'p': {
            std::shuffle(potionOdds, potionOdds + sizeof(potionOdds) / sizeof(potionOdds[0]), rng);
            std::string potion_name = potionOdds[0];
        	return new Potion(x, y, potion_name);
        }
        case 't': {
            std::shuffle(treasureOdds, treasureOdds + sizeof(treasureOdds) / sizeof(treasureOdds[0]), rng);
            std::string treasure_name = treasureOdds[0];
            if (treasure_name == "N")
            	return new Treasure(x, y, 1);
            else if (treasure_name == "S")
            	return new Treasure(x, y, 2);
            else if (treasure_name == "M")
            	return new Treasure(x, y, 4);
            else
            	return new Treasure(x, y, 6);
        }
        case 'c': {
            return new Compass(x, y);
        }
        case 'b': {
            return new BarrierSuit(x, y);
        }
    }
}

