#ifndef _DRAGON_H
#define _DRAGON_H

#include "enemy.h"
#include "treasure.h"
#include "barriersuit.h"

class Dragon : public Enemy {
    Item *booty;
    public:
        Dragon(int x, int y);
        void setHoard(Item *hoard);
        char getSymbol() const override;
        // Treasure getHoard();
        void move(int x, int y) override;
        int rewardAmount();
        void unGuardHoard();
        void die() override;
};

#endif
