#ifndef _GAME_H_
#define _GAME_H_
#include "floor.h"
#include "player.h"
#include <string>
#include <sstream>

class Game {
   
    Floor *floor;
    Player *p;
    int barrierFloor;
    int floorNumber;
    std::string actionMessage;
    bool preloadedMaps;
    std::string preloadedFloors[5];
    public:
        Game();
        Game(std::string m);
        void move(std::string dir);
        void attack(std::string dir);
        void update();
        void useItem(std::string dir);
        void restart();
        int getScore();
        void advanceFloor();
        void advanceFloorPreloaded();
        void Lose();
        void Win();
        void initPlayer();
        std::string getActionMessage();
        void resetActionMessage();
        void setActionMessage(std::string m);
        ~Game();

    friend std::ostream &operator<<(std::ostream &out, const Game &g);
};


#endif
